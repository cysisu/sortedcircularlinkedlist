package com.hackathon.cn;

public class Node {
    public int data;
    public Node next;

    public Node(int e){
        this.data = e;
    }

    public Node(){
        
    }
}
