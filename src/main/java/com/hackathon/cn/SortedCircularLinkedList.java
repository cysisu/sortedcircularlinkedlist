package com.hackathon.cn;


import java.util.LinkedList;
import java.util.List;

public class SortedCircularLinkedList {
    private Node head;
    private int size = 0;

    public SortedCircularLinkedList() {
        head = new Node();
        head.next =head;
    }

    public boolean insert(int data){
        Node newNode=new Node(data);

        //if the list's size is 0, we just insert the new node in the head
        if(size==0){
            head.next=newNode;
            newNode.next=head;
            size++;
            return true;
        }

        //if the list's size bigger than 0, we should look for the position where the new node inserts first
        Node insertedNode=head;
        while(insertedNode.next!=head){
            if(data>insertedNode.next.data){
                insertedNode=insertedNode.next;
            }
            else{
                break;
            }
        }
        newNode.next=insertedNode.next;
        insertedNode.next=newNode;
        size++;
        return true;
    }

    public int getSize(){
        return size;
    }

    public void display(){
        String res="";
        Node nowNode=head;
        while(nowNode.next!=head){
            res += String.format("%s ",nowNode.next.data);
            nowNode=nowNode.next;
        }
        System.out.println(res);
    }

    public boolean deleteAtPos(int position){
        if(position>size)
            return false;

        int nowPos=1;
        Node nowNode=head;
        while(nowNode.next!=head){
            if(nowPos==position){
                nowNode.next=nowNode.next.next;
                size--;
                return true;
            }
            nowPos++;
            nowNode=nowNode.next;
        }
        return false;
    }

    public boolean isEmpty(){
        if(size==0){
            return true;
        }
        return false;
    }
}
