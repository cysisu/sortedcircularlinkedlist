package com.hackathon.cn;

import static org.junit.jupiter.api.Assertions.*;

class SortedCircularLinkedListTest {

    private SortedCircularLinkedList list=new SortedCircularLinkedList();

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void insert() {
        list.insert(2);
        assertEquals(list.getSize(),1);
    }

    @org.junit.jupiter.api.Test
    void getSize() {
    }

    @org.junit.jupiter.api.Test
    void display() {
    }

    @org.junit.jupiter.api.Test
    void deleteAtPos() {
        list.insert(2);
        list.insert(3);
        list.deleteAtPos(1);
        list.display();
    }

    @org.junit.jupiter.api.Test
    void isEmpty() {
    }
}